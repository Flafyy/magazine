package com.ani.magazine.ctrl;

import com.ani.magazine.domain.User;
import com.ani.magazine.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;
import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    UserService uServ;

    @Autowired
    ShaPasswordEncoder encoder;

    @RequestMapping(value = {"/", "/index"})
    private String showIndex(Map<String, Object> model, @RequestParam(value = "error", required = false) String error) {
        if (error != null) {
            model.put("error", "Не правильный логин и пароль попробуйте еще раз");
        }
        return "index";
    }

    @RequestMapping(value = "/setting")
    private String showSetting() {
        return "setting";
    }
}
