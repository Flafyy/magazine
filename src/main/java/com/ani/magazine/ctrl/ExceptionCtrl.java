package com.ani.magazine.ctrl;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExceptionCtrl {

    @RequestMapping(value = "/404")
    private String get404Page(){
        return "error/errorstatus";
    }
}
