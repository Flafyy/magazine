package com.ani.magazine.services;

import com.ani.magazine.domain.User;
import com.ani.magazine.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    UserRepo uRepo;

    @Transactional
    public void save(User user) {
        uRepo.save(user);
    }

    @Transactional
    public User findByLogin(String login) {
        return uRepo.findByLogin(login);
    }


}
