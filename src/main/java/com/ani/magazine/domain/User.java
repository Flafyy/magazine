package com.ani.magazine.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "mag_user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @NotNull(message = "Имя не может быть пустым.")
    @Size(min = 5, max = 30, message = "Имя не может быть менее 3 символов и  более 30.")
    @Column(nullable = false, length = 30)
    private String name;

    @NotNull(message = "Пароль не может быть пустым.")
    @Size(min = 8, max = 64, message = "Пароль не может быть менее 8 символов и  более 30.")
    @Column(nullable = false, length = 30)
    private String pass;

    @NotNull(message = "Логин не может быть пустым.")
    @Size(min = 8, max = 30, message = "Логин не может быть менее 8 символов и  более 30.")
    @Column(nullable = false, unique = true, length = 30)
    private String login;

    @Column(nullable = true, unique = true, length = 64)
    private String token;

    @Transient
    private String rePass;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "mag_user_roles", joinColumns = {
            @JoinColumn(name = "idUser", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "idRole",
                    nullable = false, updatable = false)})
    private Set<Role> roles = new HashSet<>();

}
