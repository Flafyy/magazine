package com.ani.magazine.domain;

import lombok.Data;

import java.util.Set;

@Data
public class Category {
    private String id;
    private String name;
    private Integer percentSale;
    private Set<Product> products;
}
